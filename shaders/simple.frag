#version 330 core

uniform mat4 m;
uniform vec3 color;
uniform sampler2D shadowmap;
uniform vec3 light_pos;

in vec3 world_pos;
in vec3 pass_normal;
in vec2 pass_uv;
in vec4 frag_pos_light_space;

out vec4 FragColor;

float is_in_shadow(vec3 light_dir) {
    vec3 proj_coords = frag_pos_light_space.xyz / frag_pos_light_space.w;
    proj_coords = (proj_coords + 1) / 2; // [-1,1] -> [0,1]

    float light_pov_closest_depth = texture(shadowmap, proj_coords.xy).r;
    float light_pov_frag_depth = proj_coords.z;
    float shadow_coef = light_pov_frag_depth > light_pov_closest_depth ? 1 : 0;

    float light_surf_cos = dot(pass_normal, -light_dir);

    float bias = mix(0.0002, 0.002, 1 - max(0, light_surf_cos));

    if (light_pov_frag_depth > light_pov_closest_depth + bias) {
        return 1.0;
    } else {
        return 0.0;
    }
}

void main() {
    vec3 normal = pass_normal;
    vec2 uv = pass_uv;
    vec3 light_dir = normalize(world_pos - light_pos);

    float ambient = 0.5;

    const float table[4] = float[4](1, 0.8, 0.8, 1);
    vec2 local_uv = mod(uv * 30, 2);
    float brightness = table[int(trunc(local_uv[0])) + 2 *  int(trunc(local_uv[1]))];

    float shadow_coef = is_in_shadow(light_dir);

    float final_coef = max(ambient, 1 - shadow_coef); 
    vec3 final_color = final_coef * (brightness * color);

    FragColor = vec4(final_color, 1);
}
