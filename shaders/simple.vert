#version 330 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

uniform mat4 m;
uniform mat4 v;
uniform mat4 p;
uniform mat4 light_transform;

out vec3 world_pos;
out vec3 pass_normal;
out vec2 pass_uv;
out vec4 frag_pos_light_space;

void main() {
    gl_Position = p * v * m * vec4(pos, 1);

    world_pos = vec3(m * vec4(pos, 1));
    pass_normal = normal;
    pass_uv = uv;
    frag_pos_light_space = light_transform * vec4(world_pos, 1);
}
