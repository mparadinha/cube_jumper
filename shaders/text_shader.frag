#version 330 core

uniform sampler2D font_atlas;
uniform vec3 text_color;

in vec2 pass_uv;

out vec4 FragColor;

void main() {
    float alpha = texture(font_atlas, pass_uv).r;
    FragColor = vec4(text_color, alpha);
}
