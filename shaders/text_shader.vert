#version 330 core

layout (location = 0) in vec2 in_pos;
layout (location = 1) in vec2 in_uv;

uniform mat4 m;

out vec2 pass_uv;

void main() {
    gl_Position = m * vec4(in_pos, 0, 1);
    pass_uv = in_uv;
}
