#version 330 core

uniform sampler2D tex;

in vec2 pass_uv;

out vec4 FragColor;

void main() {
    float pixel = texture(tex, pass_uv).r;
    FragColor = vec4(vec3(pixel), 1);
}
