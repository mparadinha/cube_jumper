const std = @import("std");
const Allocator = std.mem.Allocator;

const gfx = @import("graphics.zig");
const math = @import("math.zig");
const vec2 = math.vec2;
const vec3 = math.vec3;
const vec4 = math.vec4;
const mat4 = math.mat4;
const camera = @import("camera.zig");
const physics = @import("physics.zig");
const AABB = physics.AABB;

const Player = @This();

allocator: Allocator,
aabb: AABB,
cam: camera.FPCam,
vel: vec3 = math.zeroes(vec3),
accel: vec3 = math.zeroes(vec3),

/// AABB of the objects we're connect to (ground, wall if wall-riding) or null if in the air
connected_contact: ?physics.Contact = null,

state: State = .walking,
last_state: State = .walking,
last_walk_state: State = .walking,

locked_wall_vel: vec3 = math.zeroes(vec3),

time_spent_in_slide: f32 = 0,

const wall_riding_speed: f32 = 10;

const State = enum {
    walking,
    sprinting,
    falling,
    wall_riding,
    sliding,
};

pub fn set_matrices(self: Player, shader: gfx.Shader, screen_ratio: f32) void {
    var cam = self.cam;
    if (self.state == .sliding) cam.pos[1] -= 1;
    cam.set_matrices(shader, screen_ratio);
}

pub const Inputs = struct {
    move_forward_amount: f32, // goes from -1 (go backwards, full speed) to 1 (forward at full speed)
    move_right_amount: f32, // same as above, but -1 is left, 1 is right
    sprint: Input,
    crouch: Input,
    jump: Input,

    pub const Input = struct {
        held: bool,
        pressed: bool,
        released: bool,
    };
};

pub fn update(self: *Player, dt: f32, colliders: []AABB, inputs: Inputs) void {
    // TODO:
    // if we where wall riding but no longer colliding with that wall, stop wall ride (and revert to normal velocity)

    const forward_dir = self.cam.look_dir();
    const right_dir = math.cross(forward_dir, math.axis_vec(.y));
    var move_vec = math.times(forward_dir, inputs.move_forward_amount) +
        math.times(right_dir, inputs.move_right_amount);

    //const walking_speed = 2; // in m/s
    const running_speed = 4; // in m/s
    const sprinting_speed = 8; // in m/s
    const sliding_speed = 10;

    // this functions needs to:
    // get all the collision contacts
    // switch on state (check if state needs to change given new collisions)
    // if wall riding check direction of the wall and make sure we don't move the player
    //   into the wall, but also not away from the wall
    // resolve collision that need resolving
    // update vel/pos

    const contacts = physics.generate_contacts(self.aabb, colliders, self.allocator) catch unreachable;
    defer self.allocator.free(contacts);

    var floor_contact: ?physics.Contact = null;
    var wall_contact: ?physics.Contact = null;
    //var wall2_contact: ?physics.Contact = null; // TODO: for turning when hitting corners
    for (contacts) |contact| {
        switch (contact.surface()) {
            .neg_y => floor_contact = contact,
            .pos_x, .neg_x, .pos_z, .neg_z => {
                if (wall_contact == null) wall_contact = contact;
            },
            else => {},
        }
    }

    // state changes
    const saved_state = self.state;
    switch (self.state) {
        .walking => {
            if (inputs.sprint.pressed) self.state = .sprinting;
            if (inputs.crouch.pressed) self.state = .sliding;
            if (inputs.jump.pressed) {
                if (wall_contact) |contact| {
                    self.startWallRide(contact);
                } else {
                    self.startJump();
                }
            }
        },
        .sprinting => {
            if (inputs.sprint.pressed) self.state = .walking;
            if (inputs.crouch.pressed) self.state = .sliding;
            if (inputs.jump.pressed) {
                if (wall_contact) |contact| {
                    self.startWallRide(contact);
                } else {
                    self.startJump();
                }
            }
        },
        .falling => {
            if (floor_contact != null) self.state = self.last_walk_state;
            if (wall_contact) |contact| self.startWallRide(contact);
            if ((inputs.jump.pressed or inputs.jump.held) and wall_contact != null)
                self.startWallRide(wall_contact.?);
        },
        .wall_riding => {
            if (inputs.jump.pressed) {
                self.connected_contact = null;
                self.vel = math.times(self.cam.look_dir(), math.size(self.vel));
                self.startJump();
            }
        },
        .sliding => {
            self.time_spent_in_slide += dt;
            if (self.time_spent_in_slide > 1.5 or inputs.crouch.pressed) {
                self.state = self.last_walk_state;
                self.time_spent_in_slide = 0;
            }
            if (inputs.jump.pressed) {
                self.time_spent_in_slide = 0;
                self.startJump();
            }
        },
    }
    if (self.state != saved_state) {
        self.last_state = saved_state;
        if (saved_state == .walking or saved_state == .sprinting)
            self.last_walk_state = saved_state;
    }

    // NOTE FROM PAST SELF:
    // if you keep track of the last "walking" (i.e. walk/sprint) state you can return
    // to that instead when getting out of a slide.
    // maybe theres a better way. thats up to you :)

    if (floor_contact) |contact| {
        self.aabb.center[1] += contact.penetration[1];
        self.vel[1] = 0;
        // only stop the player when touching the floor when no movement keys are pressed
        if (inputs.move_forward_amount == 0 and inputs.move_right_amount == 0 and !inputs.crouch.held)
            self.vel = math.zeroes(vec3);
    }
    if (wall_contact) |contact| {
        const i: usize = @enumToInt(contact.axis());
        switch (contact.surface()) {
            .pos_x, .pos_y, .pos_z => self.aabb.center[i] -= contact.penetration[i],
            .neg_x, .neg_y, .neg_z => self.aabb.center[i] += contact.penetration[i],
        }
        // TODO: actually we should cancel out only the projected part of the velocity on the contact axis
        self.vel[i] = 0;
    }

    if (floor_contact) |contact| self.connected_contact = contact;

    // make sure we stop wall riding when the wall ends (and also start falling if floor ends)
    if (self.state == .wall_riding or self.state == .walking) {
        if (self.connected_contact) |contact| {
            const overlap_dists = physics.axes_overlaps(self.aabb, contact.box_b);
            const still_overlaps = switch (contact.axis()) {
                .x => overlap_dists[1] != 0 and overlap_dists[2] != 0,
                .y => overlap_dists[0] != 0 and overlap_dists[2] != 0,
                .z => overlap_dists[0] != 0 and overlap_dists[1] != 0,
            };
            if (!still_overlaps) self.connected_contact = null;
        }
    }

    if (self.connected_contact == null) self.state = .falling;

    self.accel = math.zeroes(vec3);

    // apply gravity if were falling
    self.accel[1] = if (self.state == .falling) -9.8 else 0;

    // movement from player input
    switch (self.state) {
        .walking, .sprinting, .sliding => {
            const speed: f32 = switch (self.state) {
                .sprinting => sprinting_speed,
                .sliding => sliding_speed,
                else => running_speed,
            };
            const vel = math.times(math.normalize(move_vec), speed);
            self.vel[0] = vel[0];
            self.vel[2] = vel[2];
        },
        .falling => {
            // limit player movement while falling
            const accel = math.times(math.normalize(move_vec), 1);
            self.accel[0] = accel[0];
            self.accel[2] = accel[2];
        },
        .wall_riding => {},
    }

    // update velocity and position integrate velocity position
    self.vel += math.times(self.accel, dt);
    self.aabb.center += math.times(self.vel, dt);

    // tilt camera (roll)
    self.cam.roll = if (self.state == .wall_riding) blk: {
        const look_dir = self.cam.look_dir();
        const wall_dir = self.connected_contact.?.surface().as_vec();
        const cross_up = math.cross(look_dir, wall_dir)[1];
        break :blk if (cross_up > 0) @as(f32, 3) else @as(f32, -3);
    } else 0;
}

fn startJump(self: *Player) void {
    self.connected_contact = null;
    self.switchState(.falling);
    self.vel[1] += 4;
}

fn startWallRide(self: *Player, contact: physics.Contact) void {
    self.connected_contact = contact;
    self.switchState(.wall_riding);

    const axis: usize = switch (self.connected_contact.?.axis()) {
        .x => 2,
        .z => 0,
        else => unreachable,
    };
    const sign: f32 = if (self.vel[axis] < 0) -1 else 1;
    self.locked_wall_vel = math.zeroes(vec3);
    self.locked_wall_vel[axis] = sign * Player.wall_riding_speed;
    //self.locked_wall_vel[axis] = sign * std.math.fabs(self.vel[axis]);
    // TODO: only start wall riding if the speed on that axis is bigger than some threshold
    // TODO: if going too fast downwards to do wall ride do a slowing slice

    // go up slightly while wall riding
    //self.locked_wall_vel[1] = 0.5;

    self.vel = self.locked_wall_vel;
}

fn switchState(self: *Player, new_state: State) void {
    self.last_state = self.state;
    self.state = new_state;
}
