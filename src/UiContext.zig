const std = @import("std");
const Allocator = std.mem.Allocator;
const Font = @import("text.zig").Font;
const math = @import("math.zig");
const vec2 = math.vec2;
const vec3 = math.vec3;
const mat4 = math.mat4;
const gfx = @import("graphics.zig");
const Window = @import("window.zig").Window;

const Self = @This();
allocator: Allocator,
font: Font,
nodes: std.BoundedArray(Node, 1000),
node_data: std.StringHashMap(NodeData),
current_frame: u64,
text_shader: gfx.Shader,
window_size: [2]u32,
mouse_ndc: vec2,

pub fn init(allocator: Allocator) !Self {
    const font_size: u32 = 40;
    return Self{
        .allocator = allocator,
        .font = try Font.from_ttf(allocator, "VictorMono-Regular.ttf", font_size),
        .nodes = try std.BoundedArray(Node, 1000).init(0),
        .node_data = std.StringHashMap(NodeData).init(allocator),
        .current_frame = 0,
        .text_shader = gfx.Shader.from_srcs(allocator, "ui_text", .{
            .vertex = text_shader_vert_src,
            .fragment = text_shader_frag_src,
        }),
        .window_size = undefined,
        .mouse_ndc = undefined,
    };
}

pub fn deinit(self: *Self) void {
    self.font.deinit();
    self.node_data.deinit();
    self.text_shader.deinit();
}

pub const Comm = struct {
    clicked: bool,
    pressed: bool,
    hovering: bool,
};

pub fn text(self: *Self, string: []const u8) Comm {
    const node = self.makeNode(.{
        .draw_text = true,
    }, string);
    return self.getComm(node);
}

pub fn button(self: *Self, string: []const u8) Comm {
    const node = self.makeNode(.{
        .clickable = true,
        .draw_text = true,
        .draw_background = true,
    }, string);
    return self.getComm(node);
}

pub fn start_frame(self: *Self, window: *Window) void {
    window.framebuffer_size(&self.window_size[0], &self.window_size[1]);
    self.mouse_ndc = window.mouse_pos_ndc();
}

pub fn render(self: *Self) !void {
    // solve node layout
    for (self.nodes.slice()) |*node| {
        const text_rect = self.font.text_rect(node.string);
        const rect_size = text_rect.max - text_rect.min;
        node.computed_size = vec2{
            rect_size[0] / @intToFloat(f32, self.window_size[0]),
            rect_size[1] / @intToFloat(f32, self.window_size[1]),
        };
        var start_x = if (node.prev_sibling) |s| s.computed_rel_pos[1] else 0;
        node.computed_rel_pos[0] = 0;
        node.computed_rel_pos[1] = start_x + node.computed_size[1];

        // TODO: this is only true because all our nodes are siblings for now
        node.rect = .{
            .min = node.computed_rel_pos,
            .max = node.computed_rel_pos + node.computed_size,
        };
    }

    // render things
    for (self.nodes.slice()) |node| {
        if (node.flags.draw_text) {
            var text_mesh_verts = std.ArrayList(f32).init(self.allocator);
            defer text_mesh_verts.deinit();
            var text_mesh_indices = std.ArrayList(u32).init(self.allocator);
            defer text_mesh_indices.deinit();

            const quads = try self.font.build_quads(self.allocator, node.string);
            defer self.allocator.free(quads);
            for (quads) |quad| {
                const index_order = [6]u32{ 0, 1, 2, 0, 2, 3 };
                const quads_done = @intCast(u32, text_mesh_verts.items.len / 16);
                for (index_order) |idx| try text_mesh_indices.append(idx + quads_done * 4);
                for (quad.points) |vert| {
                    try text_mesh_verts.appendSlice(&[2]f32{ vert.pos[0], vert.pos[1] });
                    try text_mesh_verts.appendSlice(&[2]f32{ vert.uv[0], vert.uv[1] });
                }
            }

            const text_mesh = gfx.Mesh.init(text_mesh_verts.items, text_mesh_indices.items, &.{
                .{ .n_elems = 2 },
                .{ .n_elems = 2 },
            });
            defer text_mesh.deinit();

            self.text_shader.bind();
            self.font.texture.bind(0);
            self.text_shader.set("font_atlas", @as(i32, 0));
            self.text_shader.set("m", math.mat_mult(
                math.translation(vec3{
                    node.computed_rel_pos[0],
                    node.computed_rel_pos[1],
                    0,
                }),
                math.scale(vec3{
                    1 / @intToFloat(f32, self.window_size[0]),
                    1 / @intToFloat(f32, self.window_size[1]),
                    1,
                }),
            ));
            self.text_shader.set("text_color", vec3{ 0, 0, 0 });
            text_mesh.draw();
        }
    }

    self.current_frame += 1;
    self.nodes.resize(0) catch unreachable;
}

const Node = struct {
    flags: Flags,
    string: []const u8,
    semantic_size: [2]Size,

    /// pointers to form an n-ary tree
    first_child: ?*Node,
    last_child: ?*Node,
    parent: ?*Node,
    next_sibling: ?*Node,
    prev_sibling: ?*Node,

    computed_rel_pos: vec2,
    computed_size: vec2,
    rect: Rect,

    pub const Flags = packed struct {
        clickable: bool = false,
        draw_text: bool = false,
        draw_background: bool = false,
    };

    pub const Size = struct {
        size: union {
            content: void,
            percent_of_parent: f32,
        },
        strictness: f32 = 1.0,
    };

    pub const Rect = struct {
        min: vec2,
        max: vec2,

        pub fn contains(self: Rect, point: vec2) bool {
            std.debug.print("self={}, point={}\n", .{ self, point });
            return (self.min[0] <= point[0] and point[0] <= self.max[0] and
                self.min[1] <= point[1] and point[1] <= self.max[1]);
        }
    };
};

const NodeData = struct {
    hot_transition: f32 = 0,
    active_transition: f32 = 0,

    last_frame_touched: u64 = 0,
};

fn getNodeData(self: *Self, node: *Node) !*NodeData {
    // find depth of node in the hierarchy
    var depth: u32 = 0;
    var iter_node = node;
    while (iter_node.parent) |parent| : (iter_node = parent.parent) depth += 1;

    // find what n-th child this is
    var child_n: u32 = 0;
    iter_node = node;
    while (iter_node.prev_sibling) |sibling| : (iter_node = sibling.prev_sibling) child_n += 1;

    var buf: u8[512] = undefined;
    const key_str = std.fmt.bufPrint(buf, "{}#{}#{s}", .{ depth, child_n, node.string }) catch &buf;

    const key_val = try self.node_data.getOrPut(key_str);
    if (!key_val.found_existing) key_val.value.* = NodeData{};

    key_val.value.last_frame_touched = self.current_frame;
}

fn makeNode(self: *Self, flags: Node.Flags, string: []const u8) *Node {
    var node = Node{
        .flags = flags,
        .string = string,
        .semantic_size = [2]Node.Size{
            Node.Size{ .size = .{ .content = {} } },
            Node.Size{ .size = .{ .content = {} } },
        },

        .first_child = null,
        .last_child = null,
        .parent = null,
        .next_sibling = null,
        .prev_sibling = null,
        .computed_rel_pos = vec2{ 0, 0 },
        .computed_size = vec2{ 0, 0 },
        .rect = Node.Rect{ .min = vec2{ 0, 0 }, .max = vec2{ 0, 0 } },
    };

    const node_idx = self.nodes.len;
    self.nodes.append(node) catch unreachable;
    const slice = self.nodes.slice();

    if (node_idx > 0) {
        slice[node_idx].prev_sibling = &self.nodes.buffer[node_idx - 1];
        slice[node_idx - 1].next_sibling = &slice[node_idx];
    }

    return &self.nodes.buffer[node_idx];
}

fn getComm(self: *Self, node: *Node) Comm {
    _ = self;
    _ = node;
    return Comm{
        .clicked = false,
        .pressed = false,
        .hovering = node.rect.contains(self.mouse_ndc),
    };
}

const text_shader_vert_src =
    \\#version 330 core
    \\
    \\layout (location = 0) in vec2 in_pos;
    \\layout (location = 1) in vec2 in_uv;
    \\
    \\uniform mat4 m;
    \\
    \\out vec2 pass_uv;
    \\
    \\void main() {
    \\    gl_Position = m * vec4(in_pos, 0, 1);
    \\    pass_uv = in_uv;
    \\}
;
const text_shader_frag_src =
    \\#version 330 core
    \\
    \\uniform sampler2D font_atlas;
    \\uniform vec3 text_color;
    \\
    \\in vec2 pass_uv;
    \\
    \\out vec4 FragColor;
    \\
    \\void main() {
    \\    float alpha = texture(font_atlas, pass_uv).r;
    \\    FragColor = vec4(text_color, alpha);
    \\}
;

const quad_shader_vert_src =
    \\#version 330 core
    \\layout (location = 0) in vec2 in_pos;
    \\out vec2 uv;
    \\uniform mat4 m;
    \\
    \\void main() {
    \\    gl_Position = m * vec4(in_pos, 0, 1);
    \\    uv = in_pos;
    \\}
;
const quad_shader_frag_src =
    \\#version 330 core
    \\uniform vec4 color;
    \\uniform bool use_texture;
    \\uniform sampler2D tex;
    \\in vec2 uv;
    \\out vec4 FragColor;
    \\
    \\void main() {
    \\    if (use_texture) {
    \\        FragColor = vec4(vec3(texture(tex, uv).r), 1);
    \\    }
    \\    else {
    \\        FragColor = color;
    \\    }
    \\}
;
