const std = @import("std");
const Allocator = std.mem.Allocator;

const physics = @import("physics.zig");
const math = @import("math.zig");
const vec3 = math.vec3;
const mat4 = math.mat4;

pub const Box = struct {
    aabb: physics.AABB,
    color: vec3,

    pub fn model(self: Box) mat4 {
        return math.mat_mult(
            math.translation(self.aabb.center),
            math.scale(self.aabb.sizes),
        );
    }
};

pub const File = struct {
    filename: []u8,
    mtime: isize,

    /// whether the underlying file has been changed
    pub fn needs_update(self: *File) !bool {
        const file = std.fs.cwd().openFile(self.filename, .{}) catch |err| switch (err) {
            // sometimes when saving with VIM we get FileNotFound
            error.FileNotFound => return false,
            else => unreachable,
        };
        defer file.close();
        const stat = try std.os.fstat(file.handle);
        const mtime = stat.mtime().tv_sec;
        if (mtime > self.mtime) {
            self.mtime = mtime;
            return true;
        }
        return false;
    }

    /// caller owns the returned memory
    pub fn read_all(self: File, allocator: Allocator) ![]u8 {
        const file = try std.fs.cwd().openFile(self.filename, .{});
        defer file.close();
        const data = try file.reader().readAllAlloc(allocator, std.math.maxInt(usize));
        return data;
    }
};

pub const Level = struct {
    file: File,
    allocator: Allocator,

    pub fn init(filename: []const u8, allocator: Allocator) !Level {
        return Level{
            .file = .{
                .filename = try allocator.dupe(u8, filename),
                .mtime = 0,
            },
            .allocator = allocator,
        };
    }

    pub fn deinit(self: *Level) void {
        self.allocator.free(self.file.filename);
    }

    /// whether the underlying file has been changed
    pub fn needs_update(self: *Level) !bool {
        return self.file.needs_update();
    }

    /// caller owns the returned memory
    pub fn load_boxes(self: *Level, allocator: Allocator) ![]Box {
        const data = try self.file.read_all(allocator);
        defer self.allocator.free(data);

        var boxes = std.ArrayList(Box).init(allocator);

        var parser = Parser.init(data);
        while (parser.next()) |tk| {
            assert_tk(tk, .beginObj);
            const box = Level.parse_box(&parser);
            try boxes.append(box);
        }

        return boxes.toOwnedSlice();
    }

    fn parse_box(parser: *Parser) Box {
        var box: Box = undefined;

        while (parser.next()) |tk| {
            if (std.meta.activeTag(tk) == .endObj) break;
            assert_tk(tk, .identifier);
            _ = parser.expect_next(.beginVec);
            if (std.mem.eql(u8, tk.identifier, "center")) {
                box.aabb.center = parser.parse_vec();
            } else if (std.mem.eql(u8, tk.identifier, "sizes")) {
                box.aabb.sizes = parser.parse_vec();
            } else if (std.mem.eql(u8, tk.identifier, "color")) {
                box.color = parser.parse_vec();
            } else {
                std.debug.panic("unknown identifier {s}\n", .{tk.identifier});
            }
        }

        return box;
    }
};

pub const Config = struct {
    file: File,
    allocator: Allocator,

    pub fn init(filename: []const u8, allocator: Allocator) !Config {
        return Config{
            .file = .{
                .filename = try allocator.dupe(u8, filename),
                .mtime = 0,
            },
            .allocator = allocator,
        };
    }

    pub fn deinit(self: *Config) void {
        self.allocator.free(self.file.filename);
    }

    /// whether the underlying file has been changed
    pub fn needs_update(self: *Config) !bool {
        return self.file.needs_update();
    }

    pub const Options = struct {
        sun_pos: vec3,
        sun_yaw: f32,
        sun_pitch: f32,
        sun_fov: f32,
        sun_near: f32,
        sun_far: f32,
        player_start_pos: vec3,
    };

    pub fn load_options(self: Config) !Options {
        const data = try self.file.read_all(self.allocator);
        defer self.allocator.free(data);

        var options: Options = undefined;
        var parser = Parser.init(data);
        while (parser.next()) |tk| {
            assert_tk(tk, .identifier);
            if (std.mem.eql(u8, tk.identifier, "sun_pos")) {
                _ = parser.expect_next(.beginVec);
                options.sun_pos = parser.parse_vec();
            } else if (std.mem.eql(u8, tk.identifier, "sun_yaw")) {
                options.sun_yaw = parser.expect_next(.float).float;
            } else if (std.mem.eql(u8, tk.identifier, "sun_pitch")) {
                options.sun_pitch = parser.expect_next(.float).float;
            } else if (std.mem.eql(u8, tk.identifier, "sun_fov")) {
                options.sun_fov = parser.expect_next(.float).float;
            } else if (std.mem.eql(u8, tk.identifier, "sun_near")) {
                options.sun_near = parser.expect_next(.float).float;
            } else if (std.mem.eql(u8, tk.identifier, "sun_far")) {
                options.sun_far = parser.expect_next(.float).float;
            } else if (std.mem.eql(u8, tk.identifier, "player_start_pos")) {
                _ = parser.expect_next(.beginVec);
                options.player_start_pos = parser.parse_vec();
            } else {
                std.debug.print("unknown identifier: {s}\n", .{tk.identifier});
            }
        }

        return options;
    }
};

const Parser = struct {
    pos: usize,
    data: []const u8,

    pub fn init(data: []const u8) Parser {
        return .{ .pos = 0, .data = data };
    }

    pub const Token = union(enum) {
        beginObj: void,
        endObj: void,
        identifier: []const u8,
        beginVec: void,
        endVec: void,
        float: f32,
    };

    pub fn next(self: *Parser) ?Token {
        while (true) {
            if (self.pos == self.data.len) return null;

            const char_pos = self.pos;
            const char = self.data[char_pos];
            self.pos += 1;
            switch (char) {
                ' ', '\n', '\t', ',' => {},
                '{' => return Token{ .beginObj = {} },
                '}' => return Token{ .endObj = {} },
                '(' => return Token{ .beginVec = {} },
                ')' => return Token{ .endVec = {} },
                '#' => {
                    while (true) : (self.pos += 1) {
                        if (self.pos == self.data.len) return null;
                        if (self.data[self.pos] == '\n') break;
                    }
                },
                else => {
                    if (char >= '0' and char <= '9' or char == '-') {
                        while (true) : (self.pos += 1) {
                            if (self.pos == self.data.len) return null;
                            const c = self.data[self.pos];
                            if (c == ' ' or c == ',' or c == ')' or c == '\n') break;
                        }
                        const float = std.fmt.parseFloat(f32, self.data[char_pos..self.pos]) catch {
                            std.debug.panic("parseFloatError at pos={}\n", .{char_pos});
                        };
                        return Token{ .float = float };
                    } else {
                        while (true) : (self.pos += 1) {
                            if (self.pos == self.data.len) return null;
                            if (self.data[self.pos] == ' ') break;
                        }
                        return Token{ .identifier = self.data[char_pos..self.pos] };
                    }
                },
            }
        }
    }

    pub fn expect_next(self: *Parser, expected_tag: std.meta.Tag(Token)) Token {
        const tk = self.next().?;
        const tk_tag = std.meta.activeTag(tk);
        if (tk_tag != expected_tag) {
            std.debug.panic("found {s}, expected {s}\n", .{ @tagName(tk_tag), @tagName(expected_tag) });
        }
        return tk;
    }

    fn parse_vec(self: *Parser) vec3 {
        var vec: [3]f32 = undefined;
        var next_tk: Parser.Token = undefined;
        for (vec) |*elem| {
            next_tk = self.expect_next(.float);
            elem.* = next_tk.float;
        }
        next_tk = self.expect_next(.endVec);
        return vec;
    }
};

pub fn assert_tk(tk: Parser.Token, expected_tag: std.meta.Tag(Parser.Token)) void {
    const tk_tag = std.meta.activeTag(tk);
    if (tk_tag != expected_tag) {
        std.debug.panic("found {s}, expected {s}\n", .{ @tagName(tk_tag), @tagName(expected_tag) });
    }
}
