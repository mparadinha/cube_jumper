const std = @import("std");
const Allocator = std.mem.Allocator;

const gl = @import("gl_4v3.zig");
const c = @import("c.zig");

const Window = @import("window.zig").Window;
const gfx = @import("graphics.zig");
const math = @import("math.zig");
const vec2 = math.vec2;
const vec3 = math.vec3;
const vec4 = math.vec4;
const mat4 = math.mat4;
const camera = @import("camera.zig");
const physics = @import("physics.zig");
const AABB = physics.AABB;
const text = @import("text.zig");
const ui = @import("ui.zig");
const level_file = @import("level_file.zig");
const TrackingTimer = @import("timer.zig").TrackingTimer;
const Player = @import("Player.zig");

var timer: TrackingTimer = undefined;

pub fn main() anyerror!void {
    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{
        .stack_trace_frames = 8,
        .enable_memory_limit = true,
    }){};
    defer _ = general_purpose_allocator.detectLeaks();
    const gpa = general_purpose_allocator.allocator();

    timer = TrackingTimer.init(gpa);
    defer timer.deinit();

    var config_file = try level_file.Config.init("config.options", gpa);
    var config = try config_file.load_options();
    defer config_file.deinit();

    var width: u32 = 1400;
    var height: u32 = 840;

    // setup GLFW
    var window = Window.init(gpa, width, height, "jmp the cubez");
    window.setup_callbacks();
    defer window.deinit();
    // setup OpenGL
    try gl.load(window.handle, get_proc_address_fn);
    gl.enable(gl.DEBUG_OUTPUT);
    gl.debugMessageCallback(gl_error_callback, null);
    std.log.info("{s}", .{gl.getString(gl.VERSION)});

    // GL state that we never change
    gl.clearColor(1, 0.9, 0.8, 1);
    gl.enable(gl.CULL_FACE);
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.LINE_SMOOTH);

    const font_size: u32 = 40;
    var font = try text.Font.from_ttf(gpa, "VictorMono-Regular.ttf", font_size);
    defer font.deinit();
    const font_atlas_quad = gfx.make_quad_mesh(0.5);
    const textured_shader = gfx.Shader.from_files(gpa, "textured_screen_quad");
    defer textured_shader.deinit();

    var ui_ctx = ui.Context.init(gpa, font, &window);
    defer ui_ctx.deinit();

    const cube_mesh = gfx.make_cube_mesh(0.5);
    const outline_mesh = gfx.make_cube_mesh(0.505);
    const simple_shader = gfx.Shader.from_files(gpa, "simple");
    defer simple_shader.deinit();

    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        try std.os.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    });
    const rand = prng.random();
    _ = rand.float(f32);

    var level = try level_file.Level.init("boxes.data", gpa);
    defer level.deinit();
    var blocks = try level.load_boxes(gpa);
    defer gpa.free(blocks);

    const initial_cam_pos = vec3{ 8, 15, -15 };
    var cam = camera.FreeCam{ .pos = initial_cam_pos, .yaw = 155, .pitch = -15, .fov = 70, .near = 0.5, .far = 1000 };

    var shadowmap = gfx.ShadowMap.init(8192, gpa);
    defer shadowmap.deinit();

    // yes. 3.4m is absurd. but that makes the center (and the camera) be at 1.70m. :)
    const player_size = vec3{ 1, 3.4, 1 };
    const player_start_pos = config.player_start_pos;
    var player = Player{
        .allocator = gpa,
        .aabb = AABB{ .center = player_start_pos, .sizes = player_size },
        .cam = camera.FPCam{ .pos = player_start_pos, .fov = 70, .near = 0.1, .far = 1000 },
    };
    c.glfwSetInputMode(window.handle, c.GLFW_CURSOR, c.GLFW_CURSOR_DISABLED);

    // things we keep track of to the next frame
    var last_mouse_pos = vec2{ 0, 0 };
    var last_time = @floatCast(f32, c.glfwGetTime());
    var frame_num: u64 = 0;
    var frame_times = [_]f32{0} ** 100;
    var display_free_cam = false;
    var show_dbg_ui = false;
    var show_normal_ui = true;
    var stuck_since_frame_0 = true;
    var use_gamepad_input = false;
    var in_menu = false;

    //var new_ui = try @import("UiContext.zig").init(gpa);
    //defer new_ui.deinit();

    while (!window.should_close()) {
        //std.log.info("frame #{}", .{frame_num});
        frame_num += 1;

        window.framebuffer_size(&width, &height);
        const ratio = @intToFloat(f32, width) / @intToFloat(f32, height);

        const cur_time = @floatCast(f32, c.glfwGetTime());
        const dt = cur_time - last_time;
        last_time = cur_time;
        _ = dt;
        frame_times[frame_num % frame_times.len] = dt;

        //new_ui.start_frame(&window);
        ui_ctx.start_frame(width, height);

        if (try level.needs_update()) {
            std.debug.print("updating level\n", .{});
            gpa.free(blocks);
            blocks = try level.load_boxes(gpa);
        }
        if (try config_file.needs_update()) {
            std.debug.print("updating config\n", .{});
            config = try config_file.load_options();
            shadowmap.camera = camera.FreeCam{
                .pos = config.sun_pos,
                .yaw = config.sun_yaw,
                .pitch = config.sun_pitch,
                .fov = config.sun_fov,
                .near = config.sun_near,
                .far = config.sun_far,
            };
        }

        const mouse_pos = window.mouse_pos();
        var mouse_diff = mouse_pos - last_mouse_pos;
        last_mouse_pos = mouse_pos;

        const gamepad = window.active_gamepad();
        var left_stick = gamepad.stick(.left);
        var right_stick = gamepad.stick(.right);

        // I'm not sure what causes this particular issue, but when starting up the game
        // with my PS3 controller connected to the computer *but not turned on* (i.e.
        // by clicking the playstation-logo button) the axes all get stuck at -1.
        // TODO: the same thing happens when disconnecting the controller and
        // reconnecting it but not pressing said button
        stuck_since_frame_0 = stuck_since_frame_0 and
            left_stick[0] == -1 and left_stick[1] == -1 and
            right_stick[0] == -1 and right_stick[1] == -1;
        if (stuck_since_frame_0) {
            left_stick = math.zeroes(vec2);
            right_stick = math.zeroes(vec2);
        }

        var player_inputs = Player.Inputs{
            .move_forward_amount = 0,
            .move_right_amount = 0,
            .crouch = .{ .held = false, .pressed = false, .released = false },
            .sprint = .{ .held = false, .pressed = false, .released = false },
            .jump = .{ .held = false, .pressed = false, .released = false },
        };

        while (window.event_queue.next()) |event| {
            var remove = true;
            switch (event) {
                .KeyUp => |key| {
                    switch (key) {
                        c.GLFW_KEY_LEFT_CONTROL => player_inputs.crouch.released = true,
                        c.GLFW_KEY_LEFT_SHIFT => player_inputs.sprint.released = true,
                        c.GLFW_KEY_SPACE => player_inputs.jump.released = true,
                        c.GLFW_KEY_F => {
                            display_free_cam = !display_free_cam;
                            use_gamepad_input = !use_gamepad_input;
                        },
                        c.GLFW_KEY_G => use_gamepad_input = !use_gamepad_input,
                        c.GLFW_KEY_F1 => {
                            show_dbg_ui = !show_dbg_ui;
                            show_normal_ui = false;
                        },
                        c.GLFW_KEY_F2 => {
                            show_normal_ui = !show_normal_ui;
                            show_dbg_ui = false;
                        },
                        c.GLFW_KEY_T => {
                            player.aabb.center = cam.pos;
                            player.vel = math.zeroes(vec3);
                            player.state = .falling;
                        },
                        c.GLFW_KEY_ESCAPE => {
                            std.debug.print("toggle menu\n", .{});
                            in_menu = !in_menu;
                            window.toggle_cursor_mode();
                        },
                        else => remove = false,
                    }
                },
                .KeyDown => |key| {
                    switch (key) {
                        c.GLFW_KEY_LEFT_CONTROL => player_inputs.crouch.pressed = true,
                        c.GLFW_KEY_LEFT_SHIFT => player_inputs.sprint.pressed = true,
                        c.GLFW_KEY_SPACE => player_inputs.jump.pressed = true,
                        else => remove = false,
                    }
                },
                .GamepadDown => |button| {
                    if (!use_gamepad_input) continue;

                    if (button == c.GLFW_GAMEPAD_BUTTON_CROSS) {
                        player_inputs.jump.pressed = true;
                    } else {
                        remove = false;
                    }
                },
                else => remove = false,
            }
            if (remove) window.event_queue.removeCurrent();
        }

        if (window.key_pressed(c.GLFW_KEY_DELETE)) {
            const fresh_player = Player{
                .allocator = player.allocator,
                .aabb = player.aabb,
                .cam = player.cam,
            };
            player = fresh_player;
            player.aabb.center = config.player_start_pos;
        }

        const should_update_player = !display_free_cam and !in_menu;
        // update player
        var colliders = std.ArrayList(AABB).init(gpa);
        defer colliders.deinit();
        for (blocks) |block| try colliders.append(block.aabb);
        player_inputs.move_forward_amount = window.key_pair(c.GLFW_KEY_W, c.GLFW_KEY_S);
        player_inputs.move_right_amount = window.key_pair(c.GLFW_KEY_D, c.GLFW_KEY_A);
        player_inputs.crouch.held = window.key_pressed(c.GLFW_KEY_LEFT_CONTROL);
        player_inputs.sprint.held = window.key_pressed(c.GLFW_KEY_LEFT_SHIFT);
        player_inputs.jump.held = window.key_pressed(c.GLFW_KEY_SPACE);
        if (should_update_player) {
            player.update(dt, colliders.items, player_inputs);
        }

        // update camera
        if (display_free_cam and !in_menu) {
            cam.update_angles(mouse_diff);
            const cam_forward = window.key_pair(c.GLFW_KEY_W, c.GLFW_KEY_S);
            const cam_right = window.key_pair(c.GLFW_KEY_D, c.GLFW_KEY_A);
            const cam_up = window.key_pair(c.GLFW_KEY_SPACE, c.GLFW_KEY_LEFT_CONTROL);
            cam.update_pos(cam_forward, cam_right, cam_up);
        }
        if (should_update_player) {
            const pos = player.aabb.center;
            const look_vec = if (use_gamepad_input) right_stick else mouse_diff;
            player.cam.update(pos, look_vec);
        }

        // start rendering
        timer.start("rendering");

        // render to shadowmap
        const theta = @floatCast(f32, c.glfwGetTime()) / 7;
        //const theta: f32 = 0.9;
        var light_cam: camera.FreeCam = undefined;
        light_cam.pos = math.times(vec3{ @sin(theta), 1, @cos(theta) }, 100);
        light_cam.fov = 70;
        light_cam.near = 1;
        light_cam.far = 200;
        light_cam.point_at(math.zeroes(vec3));
        {
            gl.viewport(0, 0, @intCast(c_int, shadowmap.size), @intCast(c_int, shadowmap.size));
            defer gl.viewport(0, 0, @intCast(c_int, width), @intCast(c_int, height));
            gl.bindFramebuffer(gl.FRAMEBUFFER, shadowmap.framebuffer.fbo);
            defer gl.bindFramebuffer(gl.FRAMEBUFFER, 0);
            //gl.cullFace(gl.FRONT);
            //defer gl.enable(gl.CULL_FACE);
            //defer gl.cullFace(gl.BACK);

            gl.clear(gl.DEPTH_BUFFER_BIT);

            //shadowmap.camera.point_at(player.aabb.center);
            //shadowmap.camera = cam;
            shadowmap.camera = light_cam;
            shadowmap.shader.bind();
            shadowmap.framebuffer.texture.bind(0);
            shadowmap.camera.set_matrices(shadowmap.shader, ratio);
            shadowmap.shader.set("p", math.ortho_proj(200, 200, 1, 200));
            for (blocks) |block| {
                shadowmap.shader.set("m", block.model());
                gl.polygonMode(gl.FRONT_AND_BACK, gl.FILL);
                cube_mesh.draw();
            }
        }
        // render to normal framebuffer
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        simple_shader.bind();
        if (display_free_cam) {
            cam.set_matrices(simple_shader, ratio);
        } else {
            player.set_matrices(simple_shader, ratio);
        }
        const light_transform = math.mat_mult(
            //shadowmap.camera.get_proj_mat(ratio),
            math.ortho_proj(200, 200, 1, 200),
            shadowmap.camera.get_view_mat(),
        );
        simple_shader.set("light_pos", light_cam.pos);
        simple_shader.set("light_transform", light_transform);
        for (blocks) |block| {
            gl.polygonMode(gl.FRONT_AND_BACK, gl.FILL);
            simple_shader.set("m", block.model());
            simple_shader.set("color", block.color);
            cube_mesh.draw();
        }
        // a simple cube where the sun is
        simple_shader.bind();
        simple_shader.set("m", math.translation(light_cam.pos));
        simple_shader.set("color", math.zeroes(vec3));
        cube_mesh.draw();

        // player
        const player_mat = math.mat_mult(
            math.translation(player.aabb.center),
            math.scale(player.aabb.sizes),
        );
        simple_shader.bind();
        simple_shader.set("m", player_mat);
        gl.polygonMode(gl.FRONT_AND_BACK, gl.LINE);
        simple_shader.set("color", vec3{ 0.5, 0, 0.5 });
        outline_mesh.draw();

        // show the player arcball cam when using free cam
        if (display_free_cam) {
            cam.set_matrices(simple_shader, ratio);
            simple_shader.set("m", math.translation(player.cam.pos));
            gl.polygonMode(gl.FRONT_AND_BACK, gl.LINE);
            simple_shader.set("color", vec3{ 0.5, 0, 0.5 });
            outline_mesh.draw();
        }

        // show font atlas quad
        if (false) {
            textured_shader.bind();
            gl.activeTexture(gl.TEXTURE0);
            textured_shader.set("tex", @as(i32, 0)); // check: sample2D 'tex' is texture unit?????
            gl.bindTexture(gl.TEXTURE_2D, @intCast(u32, font.texture_id));
            const atlas_m = math.mat_mult(
                math.translation(vec3{ -0.35, 0, 0 }),
                math.scale(vec3{ 2 / ratio, 2, 1 }),
            );
            textured_shader.set("m", atlas_m);
            gl.polygonMode(gl.FRONT_AND_BACK, gl.FILL);
            font_atlas_quad.draw();
        }

        timer.stop("rendering");

        if (show_normal_ui) {
            var topleft = vec2{ -1, 1 };
            ui_ctx.text_fmt_nl("pos={d:.2}", .{player.aabb.center}, &topleft);
            ui_ctx.text_fmt_nl("vel={d:.2}", .{player.vel}, &topleft);
            const facing_vec = player.cam.look_dir();
            ui_ctx.text_fmt_nl("look_dir={d:.2}", .{facing_vec}, &topleft);
            ui_ctx.text_fmt_nl("accel={d:.2}", .{player.accel}, &topleft);
            ui_ctx.text_fmt_nl("state={s}", .{@tagName(player.state)}, &topleft);
            ui_ctx.text_fmt_nl("last_state={s}", .{@tagName(player.last_state)}, &topleft);
            ui_ctx.text_fmt_nl("last_walk_state={s}", .{@tagName(player.last_walk_state)}, &topleft);
            ui_ctx.text_fmt_nl("time_spent_in_slide={d:.2}", .{player.time_spent_in_slide}, &topleft);
            ui_ctx.text_fmt_nl("connected={s}", .{
                if (player.connected_contact) |cc| @tagName(cc.surface()) else "null",
            }, &topleft);

            ui_ctx.text_fmt_nl("light_cam.pos={d:.2}", .{light_cam.pos}, &topleft);

            ui_ctx.tex_quad(vec2{ 0.5, 0.5 }, vec2{ 0.5, -0.5 }, vec4{ 1, 1, 1, 1 }, shadowmap.framebuffer.texture.id);

            // key state diagram
            const keys = [_]struct { name: []const u8, pos: vec2, glfw_code: i32 }{
                // zig fmt: off
                .{ .name = "Q",     .pos = vec2{ -0.92, -0.80 }, .glfw_code = c.GLFW_KEY_Q },
                .{ .name = "W",     .pos = vec2{ -0.90, -0.80 }, .glfw_code = c.GLFW_KEY_W },
                .{ .name = "E",     .pos = vec2{ -0.88, -0.80 }, .glfw_code = c.GLFW_KEY_E },
                .{ .name = "A",     .pos = vec2{ -0.92, -0.84 }, .glfw_code = c.GLFW_KEY_A },
                .{ .name = "S",     .pos = vec2{ -0.90, -0.84 }, .glfw_code = c.GLFW_KEY_S },
                .{ .name = "D",     .pos = vec2{ -0.88, -0.84 }, .glfw_code = c.GLFW_KEY_D },
                .{ .name = "SHIFT", .pos = vec2{ -0.99, -0.84 }, .glfw_code = c.GLFW_KEY_LEFT_SHIFT },
                .{ .name = "CTRL",  .pos = vec2{ -0.99, -0.88 }, .glfw_code = c.GLFW_KEY_LEFT_CONTROL },
                .{ .name = "SPACE", .pos = vec2{ -0.93, -0.88 }, .glfw_code = c.GLFW_KEY_SPACE },
                // zig fmt: on
            };
            const active_color = vec4{ 1, 1, 1, 0.6 };
            const inactive_color = math.splat(vec4, 0.6);
            for (keys) |key| {
                const color = if (window.key_pressed(key.glfw_code)) active_color else inactive_color;
                _ = ui_ctx.text_box(key.name, key.pos, color);
            }
        }

        if (show_dbg_ui and !show_normal_ui) {
            timer.start("UI setup");
            defer timer.stop("UI setup");

            c.glfwSetInputMode(window.handle, c.GLFW_CURSOR, ui_ctx.mouse_mode);

            var sum_frame_times: f32 = 0;
            for (frame_times) |time| sum_frame_times += time;
            const avg_frame_time = sum_frame_times / @intToFloat(f32, frame_times.len);
            const avg_frame_rate = 1 / avg_frame_time;

            const mem_info = try fetch_proc_info(gpa);

            var topleft = vec2{ -1, 1 };
            ui_ctx.text_fmt_nl("avg fps = {d:.2}", .{avg_frame_rate}, &topleft);
            ui_ctx.text_fmt_nl(
                "GPA: total alloc'd bytes: {d:.2}MB",
                .{@intToFloat(f32, general_purpose_allocator.total_requested_bytes) / @as(f32, (1 << 20))},
                &topleft,
            );
            ui_ctx.text_fmt_nl(
                "memory usage (virtual/RAM/shared): ({d:.2}MB/{d:.2}MB/{d:.2}MB)",
                .{
                    @intToFloat(f32, mem_info.virtual_mem_size) / @as(f32, (1 << 20)),
                    @intToFloat(f32, mem_info.ram_mem_size) / @as(f32, (1 << 20)),
                    @intToFloat(f32, mem_info.shared_mem_size) / @as(f32, (1 << 20)),
                },
                &topleft,
            );
            ui_ctx.text_fmt_nl("left={d:.2} :: right={d:.2}", .{ left_stick, right_stick }, &topleft);
            ui_ctx.text_fmt_nl("window.active_gamepad_idx={}", .{window.active_gamepad_idx}, &topleft);
            ui_ctx.text_fmt_nl("window.active_gamepad().name={s}", .{window.active_gamepad().name}, &topleft);
            ui_ctx.text_fmt_nl("window.active_gamepad().state={any}", .{window.active_gamepad().state}, &topleft);

            var timers_it = timer.timers.iterator();
            var timer_entry = timers_it.next();
            while (timer_entry != null) : (timer_entry = timers_it.next()) {
                const entry = timer_entry.?;
                const timer_ptr = entry.value_ptr;
                ui_ctx.text_fmt_nl(
                    "timer '{s}': #called={}, {d:.2}% of frame time, (avg/total): {d:.4}ms/{d:.2}s",
                    .{
                        entry.key_ptr.*,
                        timer_ptr.times_called,
                        100 * timer_ptr.average_time / avg_frame_time,
                        timer_ptr.average_time * 1000,
                        timer_ptr.total_time,
                    },
                    &topleft,
                );
            }
        }

        timer.start("UI render");
        ui_ctx.render();
        timer.stop("UI render");

        //if (new_ui.text("testing...testing...").hovering) std.debug.print("1st hover\n", .{});
        //if (new_ui.text("testing...testing...").hovering) std.debug.print("2nd hover\n", .{});
        //if (new_ui.text("testing...testing...").hovering) std.debug.print("3rd hover\n", .{});
        //try new_ui.render();

        window.update();
    }
}

fn get_proc_address_fn(window: ?*c.GLFWwindow, proc_name: [:0]const u8) ?*const anyopaque {
    _ = window;
    const fn_ptr = c.glfwGetProcAddress(proc_name);
    // without this I got a "cast discards const qualifier" error
    return @intToPtr(?*opaque {}, @ptrToInt(fn_ptr));
}

fn gl_error_callback(source: u32, error_type: u32, id: u32, severity: u32, len: i32, msg: [*:0]const u8, user_param: ?*const anyopaque) callconv(.C) void {
    _ = len;
    _ = user_param;

    if (severity == gl.DEBUG_SEVERITY_NOTIFICATION) return;

    const source_str = switch (source) {
        0x824B => "SOURCE_OTHER",
        0x824A => "SOURCE_APPLICATION",
        0x8249 => "SOURCE_THIRD_PARTY",
        0x8248 => "SOURCE_SHADER_COMPILER",
        0x8247 => "SOURCE_WINDOW_SYSTEM",
        0x8246 => "SOURCE_API",
        else => unreachable,
    };
    const error_type_str = switch (error_type) {
        0x826A => "TYPE_POP_GROUP",
        0x8269 => "TYPE_PUSH_GROUP",
        0x8268 => "TYPE_MARKER",
        0x8251 => "TYPE_OTHER",
        0x8250 => "TYPE_PERFORMANCE",
        0x824F => "TYPE_PORTABILITY",
        0x824E => "TYPE_UNDEFINED_BEHAVIOR",
        0x824D => "TYPE_DEPRECATED_BEHAVIOR",
        0x824C => "TYPE_ERROR",
        else => unreachable,
    };
    const severity_str = switch (severity) {
        0x826B => "SEVERITY_NOTIFICATION",
        0x9148 => "SEVERITY_LOW",
        0x9147 => "SEVERITY_MEDIUM",
        0x9146 => "SEVERITY_HIGH",
        else => unreachable,
    };
    std.log.info("OpenGL: ({s}, {s}, {s}, id={}) {s}", .{ source_str, severity_str, error_type_str, id, msg });
}

const ProcInfo = struct {
    /// total virtual memory used by the process, in bytes
    virtual_mem_size: usize,
    /// size of resident memory (i.e. in RAM), in bytes
    ram_mem_size: usize,
    /// memory that is shared (ram_mem_size already includes this)
    shared_mem_size: usize,
};

pub fn fetch_proc_info(allocator: Allocator) !ProcInfo {
    // note: we can use /proc/self/status for a more granular look at memory sizes
    const file = try std.fs.openFileAbsolute("/proc/self/statm", .{});
    defer file.close();

    const data = try file.readToEndAlloc(allocator, 0x1000);
    defer allocator.free(data);

    var values: [3]u64 = undefined;
    var i: usize = 0;
    var tokenizer = std.mem.tokenize(u8, data, " ");
    while (i < 3) : (i += 1) {
        if (tokenizer.next()) |str| {
            //values[i] = std.fmt.parseInt(u64, str, 0) catch |err| switch (err) {
            //    std.fmt.ParseIntError.Overflow => std.math.maxInt(u64),
            //    std.fmt.ParseIntError.InvalidCharacter => 0,
            //};
            values[i] = std.fmt.parseInt(u64, str, 0) catch 0;
        } else {
            values[i] = 0;
        }
    }

    // the values we just parsed are measured in pages, not bytes
    const page_size = @intCast(u64, c.sysconf(c._SC_PAGESIZE));

    const proc_info = ProcInfo{
        .virtual_mem_size = values[0] * page_size,
        .ram_mem_size = values[1] * page_size,
        .shared_mem_size = values[2] * page_size,
    };

    return proc_info;
}
