const std = @import("std");
const Allocator = std.mem.Allocator;

const math = @import("math.zig");
const vec3 = math.vec3;

pub const AABB = struct {
    center: vec3,
    /// total size of the box in each axes
    sizes: vec3,
};

pub const Contact = struct {
    box_a: AABB,
    box_b: AABB,
    /// absolute value of penetration for each axes
    penetration: vec3,

    /// the axis of least penetration
    pub fn axis(self: Contact) enum(u8) { x, y, z } {
        const dist = self.penetration;
        if (dist[0] <= dist[1] and dist[0] <= dist[2]) return .x;
        if (dist[1] <= dist[0] and dist[1] <= dist[2]) return .y;
        if (dist[2] <= dist[0] and dist[2] <= dist[1]) return .z;
        unreachable;
    }

    const Surface = enum(u8) {
        pos_x,
        neg_x,
        pos_y,
        neg_y,
        pos_z,
        neg_z,
        pub fn as_vec(s: Surface) vec3 {
            return switch (s) {
                .pos_x => vec3{ 1, 0, 0 },
                .neg_x => vec3{ -1, 0, 0 },
                .pos_y => vec3{ 0, 1, 0 },
                .neg_y => vec3{ 0, -1, 0 },
                .pos_z => vec3{ 0, 0, 1 },
                .neg_z => vec3{ 0, 0, -1 },
            };
        }
    };
    /// the surface of `box_a` that is colliding
    pub fn surface(self: Contact) Surface {
        return switch (self.axis()) {
            .x => if (self.box_a.center[0] > self.box_b.center[0]) Surface.neg_x else Surface.pos_x,
            .y => if (self.box_a.center[1] > self.box_b.center[1]) Surface.neg_y else Surface.pos_y,
            .z => if (self.box_a.center[2] > self.box_b.center[2]) Surface.neg_z else Surface.pos_z,
        };
    }
};

/// caller owns the returned memory
pub fn generate_contacts(box_a: AABB, colliders: []AABB, allocator: Allocator) ![]Contact {
    var contacts = std.ArrayList(Contact).init(allocator);
    for (colliders) |collider| {
        const dists = axes_overlaps(box_a, collider);
        if (dists[0] != 0 and dists[1] != 0 and dists[2] != 0) {
            try contacts.append(.{ .box_a = box_a, .box_b = collider, .penetration = dists });
        }
    }
    return contacts.toOwnedSlice();
}

/// the absolute value of overlap per axis
pub fn axes_overlaps(box_a: AABB, box_b: AABB) [3]f32 {
    const center_diff = @fabs(box_a.center - box_b.center);
    const sizes_sum = math.div(box_a.sizes + box_b.sizes, 2);
    return @fabs(@minimum(math.zeroes(vec3), center_diff - sizes_sum));
}

fn overlaps(a_start: f32, a_end: f32, b_start: f32, b_end: f32, overlap_dist: *f32) bool {
    if (b_start >= a_start and b_start <= a_end) {
        overlap_dist.* = a_end - b_start; // overlap on positive side of axis -> position overlap_dist
        return true;
    }
    if (b_end >= a_start and b_start <= a_end) {
        overlap_dist.* = a_start - b_end;
        return true;
    }
    if (b_start <= a_start and b_end >= a_end) {
        overlap_dist.* = a_end - a_start;
        return true;
    }

    return false;
}

/// penetration_dist is the direction of least penetration, only has one non-zero entry
/// and the sign of it means which side of the axis it happend on
pub fn aabb_collision(a: AABB, b: AABB, penetration_dist: *vec3) bool {
    // we project the side of the boxes on each of the axis
    // and if the projections overlap in all 3 then we have
    // a collision

    var axes_overlap = [3]bool{ false, false, false };

    const a_min = a.center - math.div(a.sizes, 2);
    const a_max = a.center + math.div(a.sizes, 2);
    const b_min = b.center - math.div(b.sizes, 2);
    const b_max = b.center + math.div(b.sizes, 2);

    var overlap_distances = [3]f32{ 0, 0, 0 };
    for (axes_overlap) |*res, i| {
        var overlap_dist: f32 = undefined;
        res.* = overlaps(a_min[i], a_max[i], b_min[i], b_max[i], &overlap_dist);
        if (res.*) overlap_distances[i] = overlap_dist;
    }

    var min_overlap_axis: usize = 0;
    for (overlap_distances) |dist, i| {
        if (std.math.fabs(dist) < std.math.fabs(overlap_distances[min_overlap_axis])) min_overlap_axis = i;
    }
    penetration_dist.*[min_overlap_axis] = overlap_distances[min_overlap_axis];

    return axes_overlap[0] and axes_overlap[1] and axes_overlap[2];
}
