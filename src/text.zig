const std = @import("std");
const gl = @import("gl_4v3.zig");
const gfx = @import("graphics.zig");
const math = @import("math.zig");
const vec2 = math.vec2;

const c = @cImport({
    @cInclude("stb_rect_pack.h");
    @cInclude("stb_truetype.h");
});

const Allocator = std.mem.Allocator;

pub const Font = struct {
    allocator: Allocator,
    file_data: [:0]u8,

    size: f32,

    texture: gfx.Texture,
    texture_size: u32,
    texture_data: []u8,

    baked_char_data: [96]c.stbtt_bakedchar,

    /// call 'deinit' when you're done with the Font
    pub fn from_ttf(allocator: Allocator, filepath: []const u8, size: f32) !Font {
        const tex_size = 512;

        var self = Font{
            .allocator = allocator,
            .file_data = try std.fs.cwd().readFileAllocOptions(
                allocator,
                filepath,
                100 * (1 << 20), // 100MB
                null,
                @alignOf(u8),
                0, // need null terminator because STB expects C style string
            ),
            // this buffer needs to stay loaded until we stop using the font

            .size = size,

            .texture = undefined,
            .texture_size = tex_size,
            .texture_data = try allocator.alloc(u8, tex_size * tex_size),

            .baked_char_data = undefined,
        };

        self.bake_bitmap();

        self.texture = gfx.Texture.init(
            self.texture_size,
            self.texture_size,
            gl.RED,
            self.texture_data,
            gl.TEXTURE_2D,
            &.{
                .{ .name = gl.TEXTURE_MIN_FILTER, .value = gl.LINEAR },
                .{ .name = gl.TEXTURE_MAG_FILTER, .value = gl.LINEAR },
                .{ .name = gl.TEXTURE_WRAP_S, .value = gl.CLAMP_TO_EDGE },
                .{ .name = gl.TEXTURE_WRAP_T, .value = gl.CLAMP_TO_EDGE },
            },
        );

        return self;
    }

    pub fn deinit(self: Font) void {
        self.allocator.free(self.file_data);
        self.allocator.free(self.texture_data);
    }

    fn bake_bitmap(self: *Font) void {
        var stb_font: c.stbtt_fontinfo = undefined;
        if (c.stbtt_InitFont(&stb_font, &self.file_data[0], 0) == 0) unreachable;

        const bake_result = c.stbtt_BakeFontBitmap(
            &self.file_data[0],
            0,
            self.size,
            &self.texture_data[0],
            @intCast(c_int, self.texture_size),
            @intCast(c_int, self.texture_size),
            0x20, // char ASCII 0x20 is space ' '
            0x60, // 96 chars
            &self.baked_char_data[0],
        );
        if (bake_result == 0) unreachable; // no chars got baked, for whatever reason
        if (bake_result < 0) std.log.info("BakeFont couldn't fit {} chars", .{bake_result});
    }

    pub const Quad = packed struct {
        points: [4]Vertex,
        const Vertex = packed struct { pos: vec2, uv: vec2 };
    };

    /// caller owns returned memory
    pub fn build_quads(self: Font, allocator: Allocator, str: []const u8) ![]Quad {
        var quads = try allocator.alloc(Quad, str.len);
        var cursor = [2]f32{ 0, 0 };
        for (str) |char, i| {
            const quad = self.build_quad(char, &cursor);
            quads[i] = quad;
        }
        return quads;
    }

    pub fn text_rect(self: Font, str: []const u8) struct { min: vec2, max: vec2 } {
        var min: vec2 = vec2{ 0, 0 };
        var max: vec2 = vec2{ 0, 0 };
        var cursor = [2]f32{ 0, 0 };
        for (str) |char| {
            const quad = self.build_quad(char, &cursor);
            for (quad.points) |vert| {
                min[0] = std.math.min(min[0], vert.pos[0]);
                min[1] = std.math.min(min[1], vert.pos[1]);
                max[0] = std.math.max(max[0], vert.pos[0]);
                max[1] = std.math.max(max[1], vert.pos[1]);
            }
        }
        return .{ .min = min, .max = max };
    }

    fn build_quad(self: Font, char: u8, cursor: *[2]f32) Quad {
        var stb_quad: c.stbtt_aligned_quad = undefined;
        std.debug.assert(char != '\n');
        c.stbtt_GetBakedQuad(
            &self.baked_char_data[0],
            @intCast(i32, self.texture_size),
            @intCast(i32, self.texture_size),
            @intCast(i32, char - 0x20), // index into baked_char_data
            &cursor[0],
            &cursor[1],
            &stb_quad,
            1,
        );

        const q = stb_quad;
        const quad = Quad{ .points = [4]Quad.Vertex{
            .{ .pos = vec2{ q.x0, -q.y1 }, .uv = vec2{ q.s0, q.t1 } },
            .{ .pos = vec2{ q.x1, -q.y1 }, .uv = vec2{ q.s1, q.t1 } },
            .{ .pos = vec2{ q.x1, -q.y0 }, .uv = vec2{ q.s1, q.t0 } },
            .{ .pos = vec2{ q.x0, -q.y0 }, .uv = vec2{ q.s0, q.t0 } },
        } };

        return quad;
    }
};
