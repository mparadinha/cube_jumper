const std = @import("std");
const Allocator = std.mem.Allocator;

const c = @import("c.zig");

pub const TrackingTimer = struct {
    timers: std.StringHashMap(Timer),

    const deltas_stored: usize = 100;

    const Timer = struct {
        is_timing: bool,
        start_time: f64,
        total_time: f64,
        times_called: u64,

        deltas: [deltas_stored]f64,
        average_time: f64,
    };

    /// call 'deinit' to clean up resources
    pub fn init(allocator: Allocator) TrackingTimer {
        return .{
            .timers = std.StringHashMap(Timer).init(allocator),
        };
    }

    pub fn deinit(self: *TrackingTimer) void {
        self.timers.deinit();
    }

    pub fn start(self: *TrackingTimer, id: []const u8) void {
        const entry = self.timers.getOrPut(id) catch unreachable;
        // new entry, need to initialize it
        if (!entry.found_existing) {
            entry.value_ptr.* = .{
                .is_timing = false,
                .start_time = 0,
                .total_time = 0,
                .times_called = 0,
                .deltas = [_]f64{0} ** deltas_stored,
                .average_time = 0,
            };
        }

        entry.value_ptr.start_time = c.glfwGetTime();
        entry.value_ptr.is_timing = true;
    }
    pub fn stop(self: *TrackingTimer, id: []const u8) void {
        const entry = self.timers.getOrPut(id) catch unreachable;
        if (!entry.found_existing) unreachable;

        const timer_ptr = entry.value_ptr;
        if (!timer_ptr.is_timing) unreachable;
        const delta = c.glfwGetTime() - timer_ptr.start_time;
        timer_ptr.total_time += delta;
        timer_ptr.times_called += 1;
        timer_ptr.is_timing = false;

        timer_ptr.deltas[timer_ptr.times_called % deltas_stored] = delta;
        var delta_sum: f64 = 0;
        for (timer_ptr.deltas) |dt| delta_sum += dt;
        timer_ptr.average_time = delta_sum / @intToFloat(f64, deltas_stored);
    }
};
