const std = @import("std");
const Allocator = std.mem.Allocator;

const Window = @import("window.zig").Window;
const Font = @import("text.zig").Font;
const gfx = @import("graphics.zig");
const gl = @import("gl_4v3.zig");
const math = @import("math.zig");
const vec2 = math.vec2;
const vec3 = math.vec3;
const vec4 = math.vec4;
const mat4 = math.mat4;
const TrackingTimer = @import("timer.zig").TrackingTimer;

const c = @import("c.zig");

pub const Rect = struct {
    size: ?vec2 = null,
    snap: union(enum) {
        topleft: vec2,
        topright: vec2,
        bottomleft: vec2,
        bottomright: vec2,
        //center: vec2,
        //middletop: vec2,
        //middlebottom: vec2,
        //middleleft: vec2,
        //middleright: vec2,
    },

    pub fn get_ranges(self: Rect) struct { x: vec2, y: vec2 } {
        if (self.size) |size| {
            const bottom_left = switch (self.snap) {
                .topleft => |tl| tl - vec2{ 0, size[1] },
                .topright => |tr| tr - size,
                .bottomleft => |bl| bl,
                .bottomright => |br| br - vec2{ size[0], 0 },
            };
            const top_right = bottom_left + size;
            return .{
                .x = vec2{ bottom_left[0], top_right[0] },
                .y = vec2{ bottom_left[1], top_right[1] },
            };
        } else {
            return .{
                .x = math.zeroes(vec2),
                .y = math.zeroes(vec2),
            };
        }
    }

    pub fn contains(self: Rect, pos: vec2) bool {
        if (self.size) |size| {
            _ = size;
            const ranges = self.get_ranges();
            return ((pos[0] >= ranges.x[0] and pos[0] <= ranges.x[1]) and
                (pos[1] >= ranges.y[0] and pos[1] <= ranges.y[1]));
        } else {
            return false;
        }
    }
};

pub const Context = struct {
    allocator: Allocator,
    font: Font,
    window: *Window,
    screen_size: vec2,

    quad_mesh: gfx.Mesh,
    quad_shader: gfx.Shader,
    quad_infos: std.ArrayList(QuadInfo),
    rects: std.ArrayList(Rect),

    text_shader: gfx.Shader,
    text_infos: std.ArrayList(TextInfo),

    last_mouse_down: bool = false,
    cur_mouse_down: bool = false,
    dragging_float: bool = false,
    float_start: f32 = undefined,
    drag_start_pos: vec2 = undefined,
    mouse_mode: i32 = c.GLFW_CURSOR_NORMAL,

    const QuadInfo = struct {
        size: vec2,
        center: vec2,
        color: vec4,
        texture_id: ?u32 = null,
    };

    const TextInfo = struct {
        quads: std.ArrayList(Font.Quad),
        vert_data: std.ArrayList(f32),
        move_delta: vec2,

        pub fn init(allocator: Allocator) TextInfo {
            return TextInfo{
                .quads = std.ArrayList(Font.Quad).init(allocator),
                .vert_data = std.ArrayList(f32).init(allocator),
                .move_delta = undefined,
            };
        }
        pub fn deinit(self: TextInfo) void {
            self.quads.deinit();
            self.vert_data.deinit();
        }
    };

    /// call 'deinit' to clean up resources
    pub fn init(allocator: Allocator, font: Font, window: *Window) Context {
        return Context{
            .allocator = allocator,
            .font = font,
            .window = window,
            .screen_size = undefined,

            .quad_mesh = gfx.make_screen_quad_mesh(),
            .quad_shader = gfx.Shader.from_srcs(allocator, "ui_quad_shader", .{
                .vertex = quad_shader_vert_src,
                .fragment = quad_shader_frag_src,
            }),
            .rects = std.ArrayList(Rect).init(allocator),
            .quad_infos = std.ArrayList(QuadInfo).init(allocator),

            .text_shader = gfx.Shader.from_srcs(allocator, "ui_text_shader", .{
                .vertex = text_shader_vert_src,
                .fragment = text_shader_frag_src,
            }),
            .text_infos = std.ArrayList(TextInfo).init(allocator),
        };
    }

    pub fn deinit(self: Context) void {
        self.rects.deinit();
        self.quad_infos.deinit();
        self.quad_shader.deinit();

        for (self.text_infos.items) |text_info| text_info.deinit();
        self.text_infos.deinit();
        self.text_shader.deinit();
    }

    pub fn start_frame(self: *Context, screen_width: u32, screen_height: u32) void {
        self.last_mouse_down = self.cur_mouse_down;
        self.cur_mouse_down = (c.glfwGetMouseButton(self.window.handle, c.GLFW_MOUSE_BUTTON_LEFT) == c.GLFW_PRESS);

        self.screen_size[0] = @intToFloat(f32, screen_width);
        self.screen_size[1] = @intToFloat(f32, screen_height);
    }

    pub fn tex_quad(self: *Context, size: vec2, center: vec2, color: vec4, texture_id: ?u32) void {
        self.quad_infos.append(.{
            .size = size,
            .center = center,
            .color = color,
            .texture_id = texture_id,
        }) catch unreachable;
    }

    pub fn quad(self: *Context, size: vec2, center: vec2, color: vec4) void {
        self.quad_infos.append(.{
            .size = size,
            .center = center,
            .color = color,
            .texture_id = null,
        }) catch unreachable;
    }

    pub fn text_box(self: *Context, msg: []const u8, topleft: vec2, box_color: vec4) vec2 {
        const text_size = self.text(msg, topleft);
        const box_center = vec2{ topleft[0] + (text_size[0] / 2), topleft[1] - (text_size[1] / 2) };
        self.quad(text_size, box_center, box_color);
        return text_size;
    }

    pub fn text(self: *Context, msg: []const u8, topleft: vec2) vec2 {
        const quads = self.font.build_quads(self.allocator, msg) catch unreachable;
        defer self.allocator.free(quads);

        var text_info = TextInfo.init(self.allocator);

        text_info.quads.appendSlice(quads) catch unreachable;

        var text_min = math.zeroes(vec2);
        var text_max = math.zeroes(vec2);

        for (quads) |q| {
            for (q.points) |point| {
                for (@as([2]f32, point.pos)) |raw_n, i| {
                    const n = (raw_n / self.screen_size[i]);
                    text_min[i] = std.math.min(text_min[i], n);
                    text_max[i] = std.math.max(text_max[i], n);

                    text_info.vert_data.append(n) catch unreachable;
                }

                text_info.vert_data.appendSlice(&@as([2]f32, point.uv)) catch unreachable;
            }
        }

        //const text_tl = vec2{  = [2]f32{ text_min[0], text_max[1] } };
        //const text_br = vec2{  = [2]f32{ text_max[0], text_min[1] } };
        //text_info.move_delta = switch (rect.snap) {
        //    .topleft => |tl| vec2.minus(tl, text_tl),
        //    .topright => |tr| vec2.minus(tr, text_max),
        //    .bottomleft => |bl| vec2.minus(bl, text_min),
        //    .bottomright => |br| vec2.minus(br, text_br),
        //};
        const size = text_max - text_min;
        const tl = vec2{ 0, size[1] };
        text_info.move_delta = topleft - tl;

        self.text_infos.append(text_info) catch unreachable;

        return size;
        //return .{
        //    .size = vec2.minus(text_max, text_min),
        //    .snap = rect.snap,
        //};
    }

    pub fn text_fmt(self: *Context, comptime fmt: []const u8, args: anytype, topleft: vec2) vec2 {
        var fmt_buffer: [4096]u8 = undefined;
        const written = std.fmt.bufPrint(&fmt_buffer, fmt, args) catch unreachable;
        return self.text(written, topleft);
    }

    pub fn text_fmt_nl(self: *Context, comptime fmt: []const u8, args: anytype, topleft: *vec2) void {
        const new_topleft = self.text_fmt(fmt, args, topleft.*);
        topleft.*[1] -= new_topleft[1];
    }

    pub fn drag_float(self: *Context, data: *f32, drag_speed: f32, rect: Rect) void {
        const text_rect = self.text_fmt("{d:.2}", .{data.*}, rect);

        const ndc_pos = self.window.mouse_pos_ndc();

        if (text_rect.contains(ndc_pos) and self.mouse_pressed()) {
            self.dragging_float = true;
            self.drag_start_pos = ndc_pos;
            self.float_start = data.*;
            std.log.info("start scaling", .{});
            self.mouse_mode = c.GLFW_CURSOR_DISABLED;
        }

        if (self.dragging_float and self.mouse_released()) {
            self.dragging_float = false;
            std.log.info("stop scalling", .{});
            self.mouse_mode = c.GLFW_CURSOR_NORMAL;
        }

        // actually change the value given mouse movement in x direction
        if (self.dragging_float) {
            const dx = ndc_pos[0] - self.drag_start_pos[0];
            data.* = self.float_start + (dx * drag_speed);
        }
    }

    fn mouse_pressed(self: Context) bool {
        return (!self.last_mouse_down and self.cur_mouse_down);
    }
    fn mouse_released(self: Context) bool {
        return (self.last_mouse_down and !self.cur_mouse_down);
    }

    pub fn render(self: *Context) void {
        gl.polygonMode(gl.FRONT_AND_BACK, gl.FILL);

        // always draw on top of whatever was on screen, no matter what
        var saved_depth_func: c_int = undefined;
        gl.getIntegerv(gl.DEPTH_FUNC, &saved_depth_func);
        gl.depthFunc(gl.ALWAYS);
        defer gl.depthFunc(@intCast(c_uint, saved_depth_func));

        self.quad_shader.bind();
        for (self.quad_infos.items) |q| {
            const center_translation = q.center - math.div(q.size, 2);
            self.quad_shader.set("m", math.mat_mult(
                math.translation(vec3{ center_translation[0], center_translation[1], 0 }),
                math.scale(vec3{ q.size[0], q.size[1], 1 }),
            ));
            self.quad_shader.set("color", q.color);
            self.quad_shader.set("use_texture", (q.texture_id != null));
            if (q.texture_id) |id| {
                _ = id;
                // this comment helped me understand this a bit better
                // https://stackoverflow.com/questions/8866904/differences-and-relationship-between-glactivetexture-and-glbindtexture
                gl.activeTexture(gl.TEXTURE0);
                self.quad_shader.set("tex", @intCast(i32, 0));
            }
            self.quad_mesh.draw();
        }
        self.quad_infos.clearRetainingCapacity();

        // render text
        for (self.text_infos.items) |*text_info| {
            var indices = self.allocator.alloc(u32, text_info.quads.items.len * 6) catch unreachable;
            defer self.allocator.free(indices);
            for (text_info.quads.items) |q, i| {
                const index_order = [6]u32{ 0, 1, 2, 0, 2, 3 };
                for (index_order) |idx, j| {
                    indices[(i * 6) + j] = idx + @intCast(u32, i * q.points.len);
                }
            }

            var mesh = gfx.Mesh.init(text_info.vert_data.items, indices, &.{
                .{ .n_elems = 2 },
                .{ .n_elems = 2 },
            });
            defer mesh.deinit();

            self.text_shader.bind();
            self.font.texture.bind(0);
            self.text_shader.set("font_atlas", @as(i32, 0));
            self.text_shader.set("m", math.mat_mult(
                math.translation(vec3{ text_info.move_delta[0], text_info.move_delta[1], 0 }),
                math.scale(math.splat(vec3, 1)),
            ));
            self.text_shader.set("text_color", vec3{ 0, 0, 0 });
            mesh.draw();

            text_info.deinit();
        }
        self.text_infos.clearRetainingCapacity();

        //var test_vao: u32 = 0;
        //gl.genVertexArrays(1, &test_vao);
        //gl.bindVertexArray(test_vao);
        //var test_vbo: u32 = 0;
        //gl.genBuffers(1, &test_vbo);
        //gl.bindBuffer(gl.ARRAY_BUFFER, test_vbo);
        //const quads = self.font.build_quads(self.allocator, "A") catch unreachable;
        //defer self.allocator.free(quads);
        //const bl_pt = quads[0].points[0];
        //const tr_pt = quads[0].points[2];
        //const test_data = [_]f32{ -0.5, -0.5, 0.5, 0.5, bl_pt.uv[0], bl_pt.uv[1], tr_pt.uv[0], tr_pt.uv[1] };
        //gl.bufferData(gl.ARRAY_BUFFER, @intCast(isize, test_data.len * @sizeOf(f32)), &test_data[0], gl.STATIC_DRAW);
        //// vec2 bottom_left
        //gl.vertexAttribPointer(0, 2, gl.FLOAT, gl.FALSE, 8 * @sizeOf(f32), null);
        //gl.enableVertexAttribArray(0);
        //// vec2 top_right
        //gl.vertexAttribPointer(1, 2, gl.FLOAT, gl.FALSE, 8 * @sizeOf(f32), @intToPtr(*const anyopaque, 2 * @sizeOf(f32)));
        //gl.enableVertexAttribArray(1);
        //// vec2 bottom left uv
        //gl.vertexAttribPointer(2, 2, gl.FLOAT, gl.FALSE, 8 * @sizeOf(f32), @intToPtr(*const anyopaque, 4 * @sizeOf(f32)));
        //gl.enableVertexAttribArray(2);
        //// vec2 top right uv
        //gl.vertexAttribPointer(3, 2, gl.FLOAT, gl.FALSE, 8 * @sizeOf(f32), @intToPtr(*const anyopaque, 6 * @sizeOf(f32)));
        //gl.enableVertexAttribArray(3);

        //const test_shader = gfx.Shader.from_srcs(self.allocator, "test", .{
        //    .vertex =
        //    \\#version 330 core
        //    \\
        //    \\layout (location = 0) in vec2 attrib_bottom_left_pos;
        //    \\layout (location = 1) in vec2 attrib_top_right_pos;
        //    \\layout (location = 2) in vec2 attrib_bottom_left_uv;
        //    \\layout (location = 3) in vec2 attrib_top_right_uv;
        //    \\
        //    \\out VS_Out {
        //    \\    vec2 bottom_left_pos;
        //    \\    vec2 top_right_pos;
        //    \\    vec2 bottom_left_uv;
        //    \\    vec2 top_right_uv;
        //    \\} vs_out;
        //    \\
        //    \\void main() {
        //    \\    vs_out.bottom_left_pos = attrib_bottom_left_pos;
        //    \\    vs_out.top_right_pos = attrib_top_right_pos;
        //    \\    vs_out.bottom_left_uv = attrib_bottom_left_uv;
        //    \\    vs_out.top_right_uv = attrib_top_right_uv;
        //    \\}
        //    ,
        //    .geometry =
        //    \\#version 330 core
        //    \\
        //    \\layout (points) in;
        //    \\layout (triangle_strip, max_vertices = 6) out;
        //    \\
        //    \\in VS_Out {
        //    \\    vec2 bottom_left_pos;
        //    \\    vec2 top_right_pos;
        //    \\    vec2 bottom_left_uv;
        //    \\    vec2 top_right_uv;
        //    \\} gs_in[];
        //    \\
        //    \\out GS_Out {
        //    \\    vec2 uv;
        //    \\} gs_out;
        //    \\
        //    \\void main() {
        //    \\    gl_Position = vec4(gs_in[0].bottom_left_pos, 0, 1);
        //    \\    gs_out.uv = gs_in[0].bottom_left_uv;
        //    \\    EmitVertex();
        //    \\    gl_Position = vec4(gs_in[0].top_right_pos.x, gs_in[0].bottom_left_pos.y, 0, 1);
        //    \\    gs_out.uv = vec2(gs_in[0].top_right_uv.x, gs_in[0].bottom_left_uv.y);
        //    \\    EmitVertex();
        //    \\    gl_Position = vec4(gs_in[0].top_right_pos, 0, 1);
        //    \\    gs_out.uv = gs_in[0].top_right_uv;
        //    \\    EmitVertex();
        //    \\    EndPrimitive();
        //    \\
        //    \\    gl_Position = vec4(gs_in[0].bottom_left_pos, 0, 1);
        //    \\    gs_out.uv = gs_in[0].bottom_left_uv;
        //    \\    EmitVertex();
        //    \\    gl_Position = vec4(gs_in[0].top_right_pos, 0, 1);
        //    \\    gs_out.uv = gs_in[0].top_right_uv;
        //    \\    EmitVertex();
        //    \\    gl_Position = vec4(gs_in[0].bottom_left_pos.x, gs_in[0].top_right_pos.y, 0, 1);
        //    \\    gs_out.uv = vec2(gs_in[0].bottom_left_uv.x, gs_in[0].top_right_uv.y);
        //    \\    EmitVertex();
        //    \\    EndPrimitive();
        //    \\}
        //    ,
        //    .fragment =
        //    \\#version 330 core
        //    \\
        //    \\in GS_Out {
        //    \\    vec2 uv;
        //    \\} fs_in;
        //    \\
        //    \\uniform sampler2D text_atlas;
        //    \\
        //    \\out vec4 FragColor;
        //    \\
        //    \\void main() {
        //    \\    float alpha = texture(text_atlas, fs_in.uv).r;
        //    \\    FragColor = vec4(vec3(0), alpha);
        //    \\}
        //    ,
        //});
        //defer test_shader.deinit();

        //test_shader.bind();
        //test_shader.set("text_atlas", @as(i32, 0));
        //gl.bindVertexArray(test_vao);
        //gl.drawArrays(gl.POINTS, 0, 1);
    }
};

const text_shader_vert_src =
    \\#version 330 core
    \\
    \\layout (location = 0) in vec2 in_pos;
    \\layout (location = 1) in vec2 in_uv;
    \\
    \\uniform mat4 m;
    \\
    \\out vec2 pass_uv;
    \\
    \\void main() {
    \\    gl_Position = m * vec4(in_pos, 0, 1);
    \\    pass_uv = in_uv;
    \\}
;
const text_shader_frag_src =
    \\#version 330 core
    \\
    \\uniform sampler2D font_atlas;
    \\uniform vec3 text_color;
    \\
    \\in vec2 pass_uv;
    \\
    \\out vec4 FragColor;
    \\
    \\void main() {
    \\    float alpha = texture(font_atlas, pass_uv).r;
    \\    FragColor = vec4(text_color, alpha);
    \\}
;

const quad_shader_vert_src =
    \\#version 330 core
    \\layout (location = 0) in vec2 in_pos;
    \\out vec2 uv;
    \\uniform mat4 m;
    \\
    \\void main() {
    \\    gl_Position = m * vec4(in_pos, 0, 1);
    \\    uv = in_pos;
    \\}
;
const quad_shader_frag_src =
    \\#version 330 core
    \\uniform vec4 color;
    \\uniform bool use_texture;
    \\uniform sampler2D tex;
    \\in vec2 uv;
    \\out vec4 FragColor;
    \\
    \\void main() {
    \\    if (use_texture) {
    \\        FragColor = vec4(vec3(texture(tex, uv).r), 1);
    \\    }
    \\    else {
    \\        FragColor = color;
    \\    }
    \\}
;
